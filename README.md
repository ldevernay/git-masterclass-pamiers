# MasterClass Git


***Note*** <br>
Lorsque vous avez un doute ou une question à propos d'une commande: mettez à profit l'option --help du terminal.
Pour rappel on s'en sert comme ça :

	commande --help

Exemple :

	ls --help

	Output:
	Utilisation : ls [OPTION]... [FICHIER]...
	Afficher des renseignements sur les FICHIERs (du répertoire actuel par défaut).
	Trier les entrées alphabétiquement si aucune des options -cftuvSUX ou --sort
	ne sont utilisées.

	...


## Déroulé

Si besoin, tout au long de la journée et de votre vie de développeur:

* la cheatsheet officielle (en français s'il vous plaît) : https://github.github.com/training-kit/downloads/fr/github-git-cheat-sheet/
* une autre cheatsheet en ligne : https://www.git-tower.com/blog/git-cheat-sheet/


### I Premières étapes: initialisation et premier commit

Créez un dossier gitMasterClass qui servira pour la journée.

Initialisez un dépôt (repository) git local dans ce dossier.

**(Initialiser un dépôt n'a rien à voir avec github ou gitlab, tout se fait ici en local pour le moment)**



Ensuite :

* Créez un fichier index.html
* Mettez-y un peu de contenu, une déclaration conforme html5 au minimum :p
* Ajoutez le à la staging area
* Commitez le


<hr/>

**Attention**


Vous avez sans doute remarqué que git vous demande d'entrer un message lors d'un commit.
Seul le titre du message est obligatoire.

Les messages des commits sont très importants, ils permettront dans le futur de bien identifier le contenu de chaques modifications.

Les titres des messages peuvent être suffisant si le commit n'apporte pas beaucoup de modifications.
Pour des commit plus important, préférez un titre simple qui résume brèvement la fonction apportée puis détaillez les modifications apportées dans le corps de message du commit.

Petite astuce lors d'un commit, utilisez l'option :

	-m "titre du message du commit"

Ainsi, vous écrivez directement le titre du message du commit sans passer par l'éditeur de texte du terminal ;)

Cependant vous devrez forcément passer par l'éditeur pour écrire dans le corps du message.


<hr/>


Tout va bien ? Vous vous en sortez ?


**Félicitations** ! Vous venez d'effectuez quelques manoeuvres parmi les plus courantes dans git ! \o/

Un petit conseil, lors de ce type de manipulations pour ajouter et commiter des changements, n'hésitez pas à toujours abuser de la commande

	git status

pour connaître l'état du repository (surtout au début quand vous n'êtes pas encore tout à fait à l'aise).

<hr>


### II Première feature : branche et merge


Vous allez maintenant vous occuper d'ajouter un fichier de styles mais vous allez d'abord créer une branche à cet effet.

* Créez une branche que vous appellerez par exemple 'css' ou 'style' (pour une meilleure compréhension, on appelle souvent les branches du nom de la feature que l'on ajoute dedans)
* Basculez sur cette branche
* Créez un fichier styles.css avec un peu de contenu pour améliorer votre page
* Ajoutez le
* Commitez le

Votre feature terminée et commitée: votre branche est considérée propre.



Il est temps maintenant de fusionner la branche courante (== celle sur laquelle vous êtes situés) et la branche master.

Pour cela, vous devrez revenir sur la master pour pouvoir 'merge' les deux branches.


Vous pouvez supprimer la branche 'css' une fois le merge correctement effectué.



<hr>


C'est fait ?

***


**Bravo** ! Vous venez de développer une feature et de la fusionner avec la branche master de votre projet.
Lors de vos futurs développements, pensez à brancher souvent pour bien compartimenter les features (et eventuels bugs).

***A propos de bugs***

La correction de bugs fait souvent l'objet de branches dédiées à cet effet.

N'hésitez pas à aller sur GitLab/GitHub regarder des projets et naviguer entre les branches.
Vous comprendrez mieux les différents workflows et vous pourrez y trouver des idées d'organisations ou de bonnes pratiques à adopter ;)

<hr>


### III Et à plusieurs comment ça se passe ?


* Si ce n'est pas encore fait, créez tous un compte sur gitlab

* Mettez vous par groupe de 2, je vous conseille de faire du pair programming sur cette partie là mais vous êtes libres de vous organiser comme bon vous semble.


Il existe deux méthodes pour avoir un projet local lié à un repository sur gitlab (remote) :


#### Commencer sur Gitlab : première méthode (créateur de projet)

* Initialisez un repository sur une des machines avec quelques fichiers (index.html, styles.css, main.js par exemple)
* Vous pouvez reprendre le repo local utilisé lors des étapes précédentes si vous préférez
* Sur gitlab, créez un repo vide du même nom
* Maintenant liez le repository local au remote sur Gitlab
* Vous pouvez ensuite push votre repo local vers le distant
* N'oubliez pas d'ajouter votre collègue dans les collaborateurs du repo pour qu'il puisse également y apporter des modifications


#### Commencer sur Gitlab : seconde méthode (contributeur au projet)

* Avec le deuxième ordi, clonez le repository Gitlab. Le remote sera ainsi automatiquement défini


#### Et avec un peu de gestion de projet ?

Comme on l'a déjà vu, les hébergeurs en ligne (Github, Gitlab, Bitbucket, etc...) intègrent de nombreux outils de gestion de projet.

N'hésitez pas à regarder ce qui se fait, notamment les kanbans et la gestion des issues qui sont fréquemment utilisés.


#### Continuer sur Gitlab

Maintenant que vous connaissez les commandes de base, libre à vous de vous fixer un objectif et de l'atteindre ensemble \o/

Quelques idées en vrac :

* coder des fonctions javascript pour arriver à faire une calculatrice (4 opérations = 4 fonctions)
* créer une page qui reproduit un site
* refaire un travail déjà exécuté depuis le début de la formation en collaborant et en versionnant régulièrement
* créer une issue / répondre à une issue sur le dépôt d'un collègue
* se renseigner sur les pull requests


N'oubliez pas que l'objectif aujourd'hui reste de :

* Créer des choses chacun de votre côté sur une branche (ou à deux sur un ordi)
* Fusionner avec la branche master lorsque la feature semble correcte
* Envoyer les modifications sur Gitlab


**Pensez bien à récupérer le travail des autres lorsqu'ils l'ont envoyé avant vous sinon vous ne pourrez pas ajouter le votre sur gitlab**



### IV Derniers points important

* Le README.md : Le README est la première chose que l'on verra de votre projet. C'est dans ce fichier que vous devrez signaler les commandes pour faire tourner votre projet ainsi que pour le tester. C'est aussi l'endroit où vous décrirez l'intérêt de votre projet, ce qu'il fait et pourquoi vous l'avez initialement créé.
	Plus d'infos dans les ressources plus bas


* Les fichiers ignorés : Vous aurez parfois besoin que git ne s'occupe pas d'un fichier dans votre repository (typiquement les fichiers de configuration, les connections aux bases de données ou les 'clefs' utilisées pour certains services). Il existe alors une manip' pour indiquer clairement à git de laisser ces fichiers tranquille. Je vous laisse chercher ça ;)


* Les conflits : Les conflits apparaissent lors des commandes "merge". En effet, la plupart du temps git saura trier l'ancien code et le nouveau pour garder ce que vous voulez. Cependant, il ne saura pas le faire tout le temps et vous demandera alors de choisir vous même quelle version garder sur les zones à conflit.

La gestion des conflits est assez perturbante au début mais on s'y fait vite ne vous inquiétez pas




N'hésitez pas non plus à chercher des commandes. Git est un outil ultra puissant qui vous facilitera la vie quand vous saurez bien vous en servir.
Si jamais vous voulez faire quelque chose avec git mais sans savoir comment, demandez à internet, consultez la documentation ou demander de l'aide au terminal (toujours cette fameuse option --help).


## Ressources


### Support de présentation

* Les slides de la présentation sont disponibles à https://slides.com/djohn12/deck#/



### Documentation

* betterexplained : https://betterexplained.com/articles/a-visual-guide-to-version-control/
* adopte un git : http://adopteungit.fr/methodologie/2017/07/02/git-les-principes-de-base-pour-le-prendre-en-main.html


### Online Courses

* le cours codecademy plutôt bien fait : https://www.codecademy.com/courses/learn-git/lessons/git-workflow/exercises/hello-git
* apprendre les branches en s'amusant : https://learngitbranching.js.org/
* un repo pour tester votre connaissance de git (il y a même des badges à gagner :D) : https://github.com/git-game/git-game


### Tools & Tips

* une ressource que je recommande à toutes les sauces : https://git-scm.com/book/en/v2


* la cheatsheet officielle (en français s'il vous plaît) : https://github.github.com/training-kit/downloads/fr/github-git-cheat-sheet/
* une cheatsheet en ligne : https://www.git-tower.com/blog/git-cheat-sheet/
* plus d'info dans le terminal avec git prompt : https://github.com/git/git/blob/master/contrib/completion/git-prompt.sh
* tips for a better workflow : https://drewdevault.com/2019/02/25/Using-git-with-discipline.html#fnref:1
* mieux comprendre les différents workflow (avec exemples) : https://www.atlassian.com/git/tutorials/comparing-workflows
* nouveaux conseils et exemples de workflows : https://git-scm.com/book/en/v2/Git-Branching-Branching-Workflows
* marre de taper votre mot de passe à chaque échange avec Gitlab ? La doc de chez eux pour mettre en place une clef ssh : https://gitlab.com/help/ssh/README#locating-an-existing-ssh-key-pair
* quelques règles à suivre pour un README.md utile : https://github.com/18F/open-source-guide/blob/18f-pages/pages/making-readmes-readable.md
* un template de README.md : https://gist.github.com/PurpleBooth/109311bb0361f32d87a2



### GUI

Pour ceux qui sont un peu rebutés par la ligne de commande, il existe des interfaces graphiques pour utiliser git.
Les plus connus :

* GitKraken : https://www.gitkraken.com/
* Git-Cola : https://git-cola.github.io/
* SmartGit : https://www.syntevo.com/smartgit/

Cela peut vous aider à vous repérer au début mais l'utilisation est assez limitée lorsque vous commencez à vraiment piger comment utiliser git et que vous savez ce que vous voulez faire (ou encore mieux, ce que vous faites).